# Author Ranking

![Screenshot of the Author Ranking application](/misc/author_ranking.png "")

This project contains the code of the application presented in the paper [Dynamic Ranking of IEEE VIS Author Importance](https://urn.kb.se/resolve?urn=urn:nbn:se:lnu:diva-110952)


The application is implemented with HTML5/JavaScript/D3 and contains two application files: author_ranking.html and author_ranking.css. The data files (plain text files) are located in the separate data folder. The application needs to be served via a web server for the data loading to work, and it also needs internet connection for D3.

## Launch instructions

1. Create a new main folder for the application

2. Copy the two application files to the main folder

3. In the main folder: create a sub folder named "data" and copy all the data files to it

4. Run a web server on/from the main folder

5. Navigate a browser (preferably Chrome) to author_ranking.html
